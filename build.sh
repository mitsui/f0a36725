#!/bin/bash
set -e

export HOME=$(pwd)
WORK_DIR=$HOME
mkdir -p $WORK_DIR || true
NGINX_INSDIR=${NGINX_INSDIR:-"$(pwd)/_nginx"}
PREFIX_LIBS=${PREFIX_LIBS:-"$(pwd)/_builds"}
export PKG_CONFIG='pkg-config --static'
export PKG_CONFIG_PATH=$PREFIX_LIBS/lib/pkgconfig
export LD_LIBRARY_PATH=$PREFIX_LIBS/lib
if type curl > /dev/null 2>&1; then
  DOWNLOADER='curl -sL'
elif type wget > /dev/null 2>&1; then
  DOWNLOADER='wget -qO-'
else
  echo -e "\e[31mCan not download file: curl/wget not found\e[0m" >&2
  exit 1
fi
mkdir -p $PREFIX_LIBS/lib
ln -s lib $PREFIX_LIBS/lib64 || true

build_zlib() {
  cd $WORK_DIR
  $DOWNLOADER https://zlib.net/current/zlib.tar.gz | tar xz
  cd zlib-*
  ./configure --prefix=$PREFIX_LIBS --static
  make -j`nproc`
  make install
}

build_xml2() {
  cd $WORK_DIR
  $DOWNLOADER https://download.gnome.org/sources/libxml2/2.13/libxml2-2.13.5.tar.xz | tar xJ
  cd libxml2-*
  ./configure --prefix=$PREFIX_LIBS --enable-static --disable-shared --without-zlib --without-lzma --without-python
  make -j`nproc`
  make install
}

build_xslt() {
  cd $WORK_DIR
  $DOWNLOADER https://download.gnome.org/sources/libxslt/1.1/libxslt-1.1.42.tar.xz | tar xJ
  cd libxslt-*
  ./configure --prefix=$PREFIX_LIBS --enable-static --disable-shared --without-python --without-crypto
  make -j`nproc`
  make install
}

build_ssl() {
  cd $WORK_DIR
  git clone --single-branch --depth 1 -b openssl-3.1.5+quic https://github.com/quictls/openssl
  cd openssl
  ./config no-zlib enable-tls1_3 no-shared --prefix=$PREFIX_LIBS linux-x86_64
  make -j`nproc`
  make install_sw
}

build_libressl() {
  cd $WORK_DIR
  $DOWNLOADER https://ftp.openbsd.org/pub/OpenBSD/LibreSSL/libressl-3.9.2.tar.gz | tar xz
  cd libressl-*
  cmake -B_b -DCMAKE_BUILD_TYPE=MinSizeRel -DCMAKE_INSTALL_PREFIX=$PREFIX_LIBS -DCMAKE_PREFIX_PATH=$PREFIX_LIBS -DBUILD_SHARED_LIBS=0 -DLIBRESSL_APPS=0 -DLIBRESSL_TESTS=0 -DENABLE_NC=0
  make -j`nproc` -C_b install
}

build_geoip() {
  cd $WORK_DIR
  $DOWNLOADER https://github.com/maxmind/geoip-api-c/releases/download/v1.6.12/GeoIP-1.6.12.tar.gz | tar xz
  cd GeoIP-*
  ./configure --prefix=$PREFIX_LIBS --enable-static --disable-shared
  make -j`nproc`
  make install
}

build_jpeg() {
  cd $WORK_DIR
  $DOWNLOADER https://download.sourceforge.net/libjpeg-turbo/libjpeg-turbo-3.0.1.tar.gz | tar xz
  cd libjpeg-turbo-*
  cmake -B"$(pwd)/_b" -DCMAKE_BUILD_TYPE=Release -DENABLE_SHARED=FALSE -DENABLE_STATIC=TRUE .
  make -j`nproc` --directory="$(pwd)/_b" install DESTDIR=$PREFIX_LIBS/_libjpeg-turbo
  cp -r $PREFIX_LIBS/_libjpeg-turbo/opt/libjpeg-turbo/include/* $PREFIX_LIBS/include/
  cp -r $PREFIX_LIBS/_libjpeg-turbo/opt/libjpeg-turbo/lib*/* $PREFIX_LIBS/lib/
  sed -i "s|=/opt/libjpeg-turbo|=$PREFIX_LIBS|g" $PREFIX_LIBS/lib/pkgconfig/libturbojpeg.pc
  sed -i "s|=/opt/libjpeg-turbo|=$PREFIX_LIBS|g" $PREFIX_LIBS/lib/pkgconfig/libjpeg.pc
  rm -rf $PREFIX_LIBS/_libjpeg-turbo
}

build_png() {
  cd $WORK_DIR
  $DOWNLOADER https://download.sourceforge.net/libpng/libpng-1.6.45.tar.xz | tar xJ
  cd libpng-*
  CPPFLAGS="-I$PREFIX_LIBS/include" LDFLAGS="-L$PREFIX_LIBS/lib" ./configure --prefix=$PREFIX_LIBS --enable-static --disable-shared --enable-hardware-optimizations
  make -j`nproc`
  make install
}


build_webp() {
  cd $WORK_DIR
  # https://developers.google.com/speed/webp/download
  $DOWNLOADER https://storage.googleapis.com/downloads.webmproject.org/releases/webp/libwebp-1.5.0.tar.gz | tar xz
  cd libwebp-*
  ./configure --prefix=$PREFIX_LIBS --enable-static --disable-shared --enable-libwebpdecoder --enable-libwebpextras
  make
  make install
}

build_gd() {
  cd $WORK_DIR
  $DOWNLOADER https://github.com/libgd/libgd/releases/download/gd-2.3.3/libgd-2.3.3.tar.gz | tar xz
  cd libgd-*
  ./configure --prefix=$PREFIX_LIBS --enable-static --disable-shared --with-zlib --with-png --with-jpeg --with-webp --without-avif --without-heif --without-tiff --without-x --without-freetype --without-fontconfig
  make -j`nproc`
  make install
}

build_pcre2() {
  cd $WORK_DIR
  $DOWNLOADER https://github.com/PCRE2Project/pcre2/releases/download/pcre2-10.43/pcre2-10.43.tar.gz | tar xz
  cd pcre2-*
  ./configure --prefix=$PREFIX_LIBS --enable-static --disable-shared
  make -j`nproc`
  make install
}

build_nginx() {
  cd $WORK_DIR
  $DOWNLOADER https://github.com/arut/nginx-dav-ext-module/archive/refs/tags/v3.0.0.tar.gz | tar xz
  mv nginx-dav-ext-module-3.* nginx-dav-ext-module
  $DOWNLOADER https://nginx.org/download/nginx-1.26.2.tar.gz | tar xz
  cd nginx-1.*
  sed -i 's/-lxml2 -lxslt"/-lxml2 -lxslt -lm"/g' auto/lib/libxslt/conf
  sed -i 's/-lexslt"/-lexslt -lxml2 -lxslt -lm"/g' auto/lib/libxslt/conf
  sed -i 's/-lgd"/-lgd -ljpeg -lpng -lwebp -lsharpyuv -lz -lm -lpthread"/g' auto/lib/libgd/conf
  PATH=$PREFIX_LIBS/bin:$PATH ./configure --prefix=$NGINX_INSDIR --sbin-path=$NGINX_INSDIR/nginx \
    --conf-path=$NGINX_INSDIR/nginx.conf --modules-path=$NGINX_INSDIR/modules --error-log-path=/dev/null \
    --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --http-log-path=/dev/null \
    --build=nginx-quic --with-select_module --with-poll_module --with-threads --with-file-aio \
    --with-http_ssl_module --with-http_v2_module --with-http_v3_module --with-http_realip_module \
    --with-http_addition_module --with-http_xslt_module --with-http_image_filter_module \
    --with-http_geoip_module --with-http_sub_module --with-http_dav_module --with-http_flv_module \
    --with-http_mp4_module --with-http_gunzip_module --with-http_gzip_static_module \
    --with-http_auth_request_module --with-http_random_index_module --with-http_secure_link_module \
    --with-http_degradation_module --with-http_slice_module --with-http_stub_status_module --with-mail \
    --with-mail_ssl_module --with-stream --with-stream_ssl_module --with-stream_realip_module \
    --with-stream_geoip_module --with-stream_ssl_preread_module --add-module="$WORK_DIR/nginx-dav-ext-module" \
    --with-cc-opt="-I$PREFIX_LIBS/include -I$PREFIX_LIBS/include/libxml2" \
    --with-ld-opt="-L$PREFIX_LIBS/lib -static"
  make -j`nproc`
  make install
}

[ -f $PREFIX_LIBS/lib/libz.a ] || build_zlib
[ -f $PREFIX_LIBS/lib/libcrypto.a -a -f $PREFIX_LIBS/lib/libssl.a ] || build_libressl
[ -f $PREFIX_LIBS/lib/libxml2.a ] || build_xml2
[ -f $PREFIX_LIBS/lib/libxslt.a ] || build_xslt
[ -f $PREFIX_LIBS/lib/libpcre2-8.a ] || build_pcre2
[ -f $PREFIX_LIBS/lib/libjpeg.a ] || build_jpeg
[ -f $PREFIX_LIBS/lib/libpng.a ] || build_png
[ -f $PREFIX_LIBS/lib/libwebp.a -a -f $PREFIX_LIBS/lib/libsharpyuv.a ] || build_webp
[ -f $PREFIX_LIBS/lib/libgd.a ] || build_gd
[ -f $PREFIX_LIBS/lib/libGeoIP.a ] || build_geoip

build_nginx
strip -s -x $NGINX_INSDIR/nginx
mv $NGINX_INSDIR/nginx $HOME/nginx/nginx
